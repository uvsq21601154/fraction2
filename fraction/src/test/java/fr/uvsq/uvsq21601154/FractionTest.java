package fr.uvsq.uvsq21601154;

import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {
    @Test
    public void ConstructeurDenomNum(){
        Fraction f = new Fraction(5,10);
        assertEquals(f.getNumerateur(), 5);
        assertEquals(f.getDenominateur(), 10);
    }
    @Test
    public void ConstructeurNumerateur(){
        Fraction f1=new Fraction(2);
        assertTrue(f1.getNumerateur()==2);
    }
    
    @Test
    public void ConstructeurSansArgument(){
        Fraction f2=new Fraction();
        assertEquals(f2.getDenominateur() , 1);
        assertEquals(f2.getNumerateur() , 0);
    }

    @Test
    public void ConstantesTeste() {
        Fraction f1 = Fraction.ZERO;
        Fraction f2 = Fraction.UN;
        assertEquals(f1.getDenominateur(), 1);
        assertEquals(f1.getNumerateur(), 0);
        assertEquals(f2.getDenominateur(), 1);
        assertEquals(f2.getNumerateur(), 1);

    }
    @Test
    public void FractionToString() {
        Fraction f = new Fraction(6, 1);
        assertEquals(f.toString(), "(6/1)");
    }

    @Test
    public void consultFloat(){
        Fraction f2=new Fraction(10,2);
        assertTrue(f2.calculfraction()==5.00);


    }
}