package fr.uvsq.uvsq21601154;

public class Fraction {
    private final int numerateur;
    private final int denominateur;

    public Fraction(int numerateur, int denominateur) {
        this.numerateur = numerateur;
        this.denominateur = denominateur;
    }

    public Fraction() {
        this.numerateur = 0;
        this.denominateur = 1;
    }

    public Fraction(int numerateur) {
        this.numerateur = numerateur;
        this.denominateur = 1;
    }

    public int getNumerateur() {
        return numerateur;
    }

    public int getDenominateur() {
        return denominateur;

    }

    public static final Fraction ZERO=new Fraction(0,1);
    public static final Fraction UN=new Fraction(1,1);


    public String toString() {
        return "(" + numerateur + "/" + denominateur + ")";}

    double calculfraction() {
        return this.numerateur / this.denominateur;
    }

}
